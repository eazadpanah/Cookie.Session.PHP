<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="css/styles.css">
</head>
<body>
<div>
    <table id="MYLayout" align="center" cellpadding="0" cellspacing="0" border="1">
        <tr>
            <td id="MYHeader" colspan="2">
                Header
            </td>
        </tr>
        <tr>
            <td id="MYSubHeader" colspan="2">
                SubHeader
            </td>
        </tr>
        <tr>
            <td id="MYMain">
                Main content
                <br><br>
                <form action="?" method="get">
                    <input type="text" name="city" placeholder="enter city name : "><br><br>
                    <input type="submit" name="search"><br><br>
                </form>
                <?php
                $city = $_GET['city'];
                if (!empty($city)) {
                    $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22$city%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
                    $result = file_get_contents($url);
                    $weatherArrays = json_decode($result, true);
                    $temp = $weatherArrays['query']['results']['channel']['item']['condition']['temp'];
                    echo "temp of $city is $temp F";
                    echo "<br>";
                    $c = ($temp - 32) / 1.8;
                    echo "temp of $city is $c C";
                }
                ?>

            </td>

            <td id="MYMenu">
                Menu
                <?php
                include "menu";
                ?>
            </td>

        </tr>
        <tr>
            <td id="MYSubFooter" colspan="2">
                SubFooter
            </td>
        </tr>
        <tr>
            <td id="MYFooter" colspan="2">
                Footer
            </td>
        </tr>
    </table>
</div>
</body>
</html>
